package org.giri._6;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	
	public static void main(String[] args) {
		
		SalaryAccount salaryAccount = new SalaryAccount();
		salaryAccount.setBankName("CITI");
		salaryAccount.setSalary(1000L);
		
		EmployeeAccountOneToOne accountOneToOne = new EmployeeAccountOneToOne();
		accountOneToOne.setName("Girish");
		accountOneToOne.setType("Permanet");
		
		accountOneToOne.setSalaryAccount(salaryAccount);
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(accountOneToOne);
		//session.save(salaryAccount); // if CascadeType.All then this is not required
		
		session.getTransaction().commit();
		session.close();
		
		sessionFactory.close();
		
		
	}

}
