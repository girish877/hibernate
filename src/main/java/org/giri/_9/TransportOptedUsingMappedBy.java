package org.giri._9;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class TransportOptedUsingMappedBy implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String modeType;
	private Integer fare;
	
	@ManyToOne
	@JoinColumn(name="eid")
	private EmployeeOneToManyOrManyToOneUsingMappedBy manyOrManyToOneUsingMappedBy;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getModeType() {
		return modeType;
	}

	public void setModeType(String modeType) {
		this.modeType = modeType;
	}

	public Integer getFare() {
		return fare;
	}

	public void setFare(Integer fare) {
		this.fare = fare;
	}
	
	public EmployeeOneToManyOrManyToOneUsingMappedBy getManyOrManyToOneUsingMappedBy() {
		return manyOrManyToOneUsingMappedBy;
	}

	public void setManyOrManyToOneUsingMappedBy(EmployeeOneToManyOrManyToOneUsingMappedBy manyOrManyToOneUsingMappedBy) {
		this.manyOrManyToOneUsingMappedBy = manyOrManyToOneUsingMappedBy;
	}
}
