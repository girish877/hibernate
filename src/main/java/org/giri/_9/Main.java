package org.giri._9;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {

	public static void main(String[] args) {
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		EmployeeOneToManyOrManyToOneUsingMappedBy employeeOneToManyOrManyToOne = new EmployeeOneToManyOrManyToOneUsingMappedBy();
		employeeOneToManyOrManyToOne.setName("Gireesh");
		
		TransportOptedUsingMappedBy opted1 = new TransportOptedUsingMappedBy();
		opted1.setModeType("Bus");
		opted1.setFare(2200);
		opted1.setManyOrManyToOneUsingMappedBy(employeeOneToManyOrManyToOne);
		
		TransportOptedUsingMappedBy opted2 = new TransportOptedUsingMappedBy();
		opted2.setModeType("Cab");
		opted2.setFare(4000);
		opted2.setManyOrManyToOneUsingMappedBy(employeeOneToManyOrManyToOne);
		
		TransportOptedUsingMappedBy opted3 = new TransportOptedUsingMappedBy();
		opted3.setModeType("Own");
		opted3.setFare(0);
		opted3.setManyOrManyToOneUsingMappedBy(employeeOneToManyOrManyToOne);opted1.setManyOrManyToOneUsingMappedBy(employeeOneToManyOrManyToOne);
		
		employeeOneToManyOrManyToOne.getTrList().add(opted1);
		employeeOneToManyOrManyToOne.getTrList().add(opted2);
		employeeOneToManyOrManyToOne.getTrList().add(opted3);
		
		session.save(employeeOneToManyOrManyToOne);
		
		session.getTransaction().commit();
		session.close();
		
		sessionFactory.close();
				
	}

}
