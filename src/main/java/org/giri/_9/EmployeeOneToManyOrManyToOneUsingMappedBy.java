package org.giri._9;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class EmployeeOneToManyOrManyToOneUsingMappedBy implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer eid;
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="manyOrManyToOneUsingMappedBy")
	private List<TransportOptedUsingMappedBy> trList = new ArrayList<>();

	public Integer getEid() {
		return eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TransportOptedUsingMappedBy> getTrList() {
		return trList;
	}

	public void setTrList(List<TransportOptedUsingMappedBy> trList) {
		this.trList = trList;
	}
	
}
