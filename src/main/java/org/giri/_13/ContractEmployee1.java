package org.giri._13;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="CEMPLOYEE_TPR")
@AttributeOverrides({  
    @AttributeOverride(name="cid", column=@Column(name="cid")),  
    @AttributeOverride(name="name", column=@Column(name="name"))  
})  
public class ContractEmployee1 extends EmployeeInheritanceTablePerClass{

	private String jobType;
	private String isInsuranse;
	private String isPF;
	private String bonous;

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getIsInsuranse() {
		return isInsuranse;
	}

	public void setIsInsuranse(String isInsuranse) {
		this.isInsuranse = isInsuranse;
	}

	public String getIsPF() {
		return isPF;
	}

	public void setIsPF(String isPF) {
		this.isPF = isPF;
	}

	public String getBonous() {
		return bonous;
	}

	public void setBonous(String bonous) {
		this.bonous = bonous;
	}

}
