package org.giri._13;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	
	public static void main(String[] args) {
		
		EmployeeInheritanceTablePerClass employee = new EmployeeInheritanceTablePerClass();
		employee.setLocation("Bangalore");
		employee.setPincode(560004L);
		employee.setName("Gireesh");
		
		P1Employee pEmployee = new P1Employee();
		pEmployee.setName("Girish Kaltippi");
		pEmployee.setJobType("Permenent");
		pEmployee.setBonous("true");
		pEmployee.setIsInsuranse("true");
		pEmployee.setIsPF("true");
		
		
		ContractEmployee1 cEmployee = new ContractEmployee1();
		cEmployee.setName("Giri");
		cEmployee.setJobType("Contract");
		cEmployee.setBonous("false");
		cEmployee.setIsInsuranse("false");
		cEmployee.setIsPF("false");
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(employee);
		session.save(pEmployee);
		session.save(cEmployee);
		
		session.getTransaction().commit();
		session.close();
		
		
		sessionFactory.close();
		
		
	}

}
