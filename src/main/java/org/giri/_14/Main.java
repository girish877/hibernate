package org.giri._14;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	
	public static void main(String[] args) {
		
		EmployeeInheritanceJoined employee = new EmployeeInheritanceJoined();
		employee.setLocation("Bangalore");
		employee.setPincode(560004L);
		employee.setName("Gireesh");
		
		P2Employee pEmployee = new P2Employee();
		pEmployee.setName("Girish Kaltippi");
		pEmployee.setJobType("Permenent");
		pEmployee.setBonous("true");
		pEmployee.setIsInsuranse("true");
		pEmployee.setIsPF("true");
		
		
		ContractEmployee2 cEmployee = new ContractEmployee2();
		cEmployee.setName("Giri");
		cEmployee.setJobType("Contract");
		cEmployee.setBonous("false");
		cEmployee.setIsInsuranse("false");
		cEmployee.setIsPF("false");
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(employee);
		session.save(pEmployee);
		session.save(cEmployee);
		
		session.getTransaction().commit();
		session.close();
		
		
		sessionFactory.close();
		
		
	}

}
