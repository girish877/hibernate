package org.giri._2;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
public class EmployeeAutoInc {
	
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Integer id;
	
	@Temporal(TemporalType.DATE)
	private Date regDate;
	
	
	private String name;
	
	@Transient
	private String password;
	
	@Lob
	private String description;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getRegDate() {
		return regDate;
	}


	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	

	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return "EmployeeAutoInc [id=" + id + ", regDate=" + regDate + ", name=" + name + ", password=" + password
				+ ", description=" + description + "]";
	}


	
	
}
