package org.giri._2;

import java.util.GregorianCalendar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	public static void main(String[] args) {
		
		EmployeeAutoInc employee = new EmployeeAutoInc();
		
		employee.setName("Girish");
		
		employee.setRegDate(GregorianCalendar.getInstance().getTime());
		
		employee.setPassword("xxxxxxxx");
		
		
		employee.setDescription("long description - db type - longText");
		
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(employee);
		
		//session.getTransaction().commit();
		
		//session.close();
		
		
		
		System.out.println("Retrieving from DataBase ");
		
		//session.beginTransaction();
		EmployeeAutoInc employeeFromDB = session.get(EmployeeAutoInc.class, 1);
		System.out.println(employeeFromDB);
		
		session.getTransaction().commit();
		
		session.close();
		
		
		
		
		
		
		sessionFactory.close();
		
	}
}
