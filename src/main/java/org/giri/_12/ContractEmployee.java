package org.giri._12;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="CEMPLOYEE")
public class ContractEmployee extends EmployeeSingleInheritance{

	private String jobType;
	private String isInsuranse;
	private String isPF;
	private String bonous;

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getIsInsuranse() {
		return isInsuranse;
	}

	public void setIsInsuranse(String isInsuranse) {
		this.isInsuranse = isInsuranse;
	}

	public String getIsPF() {
		return isPF;
	}

	public void setIsPF(String isPF) {
		this.isPF = isPF;
	}

	public String getBonous() {
		return bonous;
	}

	public void setBonous(String bonous) {
		this.bonous = bonous;
	}

}
