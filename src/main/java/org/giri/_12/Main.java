package org.giri._12;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	
	public static void main(String[] args) {
		
		EmployeeSingleInheritance employee = new EmployeeSingleInheritance();
		employee.setLocation("Bangalore");
		employee.setPincode(560004L);
		employee.setName("Gireesh");
		
		PEmployee pEmployee = new PEmployee();
		pEmployee.setName("Girish Kaltippi");
		pEmployee.setJobType("Permenent");
		pEmployee.setBonous("true");
		pEmployee.setIsInsuranse("true");
		pEmployee.setIsPF("true");
		
		
		ContractEmployee cEmployee = new ContractEmployee();
		cEmployee.setName("Giri");
		cEmployee.setJobType("Contract");
		cEmployee.setBonous("false");
		cEmployee.setIsInsuranse("false");
		cEmployee.setIsPF("false");
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(employee);
		session.save(pEmployee);
		session.save(cEmployee);
		
		session.getTransaction().commit();
		session.close();
		
		
		sessionFactory.close();
		
		
	}

}
