package org.giri._10;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	
	public static void main(String[] args) {
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		EmployeeManyToManyUsingJoinTable employeeManyToMany = new EmployeeManyToManyUsingJoinTable();
		DepartmentManyToManyUsingJoinTable department = new DepartmentManyToManyUsingJoinTable();
		
		List<EmployeeManyToManyUsingJoinTable> listE = new ArrayList<>();
		List<DepartmentManyToManyUsingJoinTable> listD = new ArrayList<>();
		
		
		employeeManyToMany.setName("Gireesh");
		listE.add(employeeManyToMany);
		
		department.setdName("IVS");
		listD.add(department);
		
		employeeManyToMany.setList(listD);
		department.setList(listE);
		
		session.save(employeeManyToMany);
		
		
		session.getTransaction().commit();
		
		session.close();
		
		
		sessionFactory.close();
		
	}

}
