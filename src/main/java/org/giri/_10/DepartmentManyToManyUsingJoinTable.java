package org.giri._10;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class DepartmentManyToManyUsingJoinTable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer did;
	
	private String dName;
	
	@ManyToMany
	@JoinTable(
			name="Employee_M2M_Department", 
			joinColumns = {@JoinColumn(name="did")}, 
			inverseJoinColumns={@JoinColumn(name = "eid")} )
	private List<EmployeeManyToManyUsingJoinTable> listD = new ArrayList<>();

	public Integer getDid() {
		return did;
	}

	public void setDid(Integer did) {
		this.did = did;
	}

	public String getdName() {
		return dName;
	}

	public void setdName(String dName) {
		this.dName = dName;
	}

	public List<EmployeeManyToManyUsingJoinTable> getList() {
		return listD;
	}

	public void setList(List<EmployeeManyToManyUsingJoinTable> list) {
		this.listD = list;
	}
}
