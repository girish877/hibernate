package org.giri._10;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class EmployeeManyToManyUsingJoinTable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer eid;
	
	private String name;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name="Employee_M2M_Department", 
			joinColumns = {@JoinColumn(name="eid")}, 
			inverseJoinColumns={@JoinColumn(name = "did")} )
	private List<DepartmentManyToManyUsingJoinTable> list = new ArrayList<>();

	public Integer getEid() {
		return eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DepartmentManyToManyUsingJoinTable> getList() {
		return list;
	}

	public void setList(List<DepartmentManyToManyUsingJoinTable> list) {
		this.list = list;
	}
}
