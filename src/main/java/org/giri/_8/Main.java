package org.giri._8;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {

	public static void main(String[] args) {

		TransportOptedUsingJoinTable opted1 = new TransportOptedUsingJoinTable();
		opted1.setModeType("Bus");
		opted1.setFare(2200);
		
		
		TransportOptedUsingJoinTable opted2 = new TransportOptedUsingJoinTable();
		opted2.setModeType("Cab");
		opted2.setFare(4000);
		
		TransportOptedUsingJoinTable opted3 = new TransportOptedUsingJoinTable();
		opted3.setModeType("Own");
		opted3.setFare(0);
		
		
		List<TransportOptedUsingJoinTable> list = new ArrayList<>();
		list.add(opted1);
		list.add(opted2);
		list.add(opted3);
		
		EmployeeOneToManyOrManyToOneUsingJoinTable employeeOneToManyOrManyToOne = new EmployeeOneToManyOrManyToOneUsingJoinTable();
		employeeOneToManyOrManyToOne.setName("Gireesh");
		employeeOneToManyOrManyToOne.setTrList(list);
//		employeeOneToManyOrManyToOne.getTrList().add(opted1);
//		employeeOneToManyOrManyToOne.getTrList().add(opted2);
//		employeeOneToManyOrManyToOne.getTrList().add(opted3);
//		
//		
//		opted1.setEmployeeOneToManyOrManyToOne(employeeOneToManyOrManyToOne);
//		opted2.setEmployeeOneToManyOrManyToOne(employeeOneToManyOrManyToOne);
//		opted3.setEmployeeOneToManyOrManyToOne(employeeOneToManyOrManyToOne);
		
		
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(employeeOneToManyOrManyToOne);
		
		session.getTransaction().commit();
		session.close();
		
		sessionFactory.close();
				
	}

}
