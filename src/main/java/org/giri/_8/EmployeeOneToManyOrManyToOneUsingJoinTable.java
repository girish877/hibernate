package org.giri._8;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

@Entity
public class EmployeeOneToManyOrManyToOneUsingJoinTable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer eid;
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name="EMPLOYEE_TRANSPORT", joinColumns={@JoinColumn(name ="eid")} , inverseJoinColumns = {@JoinColumn(name="id")})
	private List<TransportOptedUsingJoinTable> trList = new ArrayList<>();

	public Integer getEid() {
		return eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TransportOptedUsingJoinTable> getTrList() {
		return trList;
	}

	public void setTrList(List<TransportOptedUsingJoinTable> trList) {
		this.trList = trList;
	}
	
}
