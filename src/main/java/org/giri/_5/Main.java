package org.giri._5;

import java.util.GregorianCalendar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {

	public static void main(String[] args) {
		
		LoginDetails loginDetails = new LoginDetails();
		
		loginDetails.setUserName("giri");
		loginDetails.setPassword("xxxxx");
		loginDetails.setRegDate(GregorianCalendar.getInstance().getTime());
		loginDetails.setIp("127.0.0.1");
		
		EmployeeEmbadedId embadedId = new EmployeeEmbadedId();
		embadedId.setfName("Gireesh");
		embadedId.setlName("Kalatippi");
		embadedId.setLoginDetails(loginDetails);
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(embadedId);
		
		session.getTransaction().commit();
		
		session.close();
		
		sessionFactory.close();
		
		
	}

}
