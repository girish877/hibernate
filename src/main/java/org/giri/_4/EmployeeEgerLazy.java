package org.giri._4;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;

@Entity
public class EmployeeEgerLazy {

	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	private Integer id;
	
	private String name;
	
	@ElementCollection(fetch=FetchType.EAGER)
	@JoinTable(name="Employee_Add")
	List<Address> addressList = new ArrayList<>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Address> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}

	@Override
	public String toString() {
		return "EmployeeEgerLazy [id=" + id + ", name=" + name + ", addressList=" + addressList + "]";
	}
	
	
	
	
}
