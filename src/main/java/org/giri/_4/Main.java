package org.giri._4;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	
	public static void main(String[] args) {
		
		Address address1 = new Address();
		address1.setDoorNo("C1305");
		address1.setStreetName("ETA THE GARDENS");
		address1.setZipcode(560023l);
		
		Address address2 = new Address();
		address2.setDoorNo("115");
		address2.setStreetName("KHB");
		address2.setZipcode(586103l);
		
		Address address3 = new Address();
		address3.setDoorNo("BB8th");
		address3.setStreetName("Evry");
		address3.setZipcode(560004l);
		
		EmployeeEgerLazy employeeEmbaded = new EmployeeEgerLazy();
		employeeEmbaded.setName("Gireesh");
		employeeEmbaded.getAddressList().add(address1);
		employeeEmbaded.getAddressList().add(address2);
		employeeEmbaded.getAddressList().add(address3);
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		session.save(employeeEmbaded);
		
		session.getTransaction().commit();
		
		employeeEmbaded = null;
		
		session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		EmployeeEgerLazy egerLazy = session.get(EmployeeEgerLazy.class, 4);
		
		System.out.println(egerLazy);
		
		session.getTransaction().commit();
		
		session.close();
		
		sessionFactory.close();
	}

}
