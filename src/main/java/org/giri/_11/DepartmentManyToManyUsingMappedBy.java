package org.giri._11;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="DMTM")
public class DepartmentManyToManyUsingMappedBy {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer did;
	
	private String dName;
	
	@ManyToMany
	@JoinColumn(name = "eid")
	private List<EmployeeManyToManyUsingMappedBy> listD = new ArrayList<>();

	public Integer getDid() {
		return did;
	}

	public void setDid(Integer did) {
		this.did = did;
	}

	public String getdName() {
		return dName;
	}

	public void setdName(String dName) {
		this.dName = dName;
	}

	public List<EmployeeManyToManyUsingMappedBy> getList() {
		return listD;
	}

	public void setList(List<EmployeeManyToManyUsingMappedBy> list) {
		this.listD = list;
	}
}
