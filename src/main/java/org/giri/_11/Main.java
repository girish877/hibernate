package org.giri._11;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	
	public static void main(String[] args) {
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		EmployeeManyToManyUsingMappedBy employeeManyToMany = new EmployeeManyToManyUsingMappedBy();
		DepartmentManyToManyUsingMappedBy department = new DepartmentManyToManyUsingMappedBy();
		
		List<EmployeeManyToManyUsingMappedBy> listE = new ArrayList<>();
		List<DepartmentManyToManyUsingMappedBy> listD = new ArrayList<>();
		
		
		employeeManyToMany.setName("Gireesh");
		listE.add(employeeManyToMany);
		
		department.setdName("IVS");
		listD.add(department);
		
		employeeManyToMany.setList(listD);
		department.setList(listE);
		
		session.save(employeeManyToMany);
		
		
		session.getTransaction().commit();
		
		session.close();
		
		
		sessionFactory.close();
		
	}

}
