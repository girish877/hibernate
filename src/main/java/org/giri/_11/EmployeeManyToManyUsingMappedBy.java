package org.giri._11;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="EMTMUM")
public class EmployeeManyToManyUsingMappedBy {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer eid;
	
	private String name;
	
	@ManyToMany(cascade = CascadeType.ALL,mappedBy="listD")
	private List<DepartmentManyToManyUsingMappedBy> list = new ArrayList<>();

	public Integer getEid() {
		return eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DepartmentManyToManyUsingMappedBy> getList() {
		return list;
	}

	public void setList(List<DepartmentManyToManyUsingMappedBy> list) {
		this.list = list;
	}
}
