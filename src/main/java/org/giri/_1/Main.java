package org.giri._1;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	public static void main(String[] args) {
		Employee e1 = new Employee();
		e1.setEmpId(1);
		e1.setEmpName("Gireesh");
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(e1);
		session.getTransaction().commit();
		
		
		
	}
}
