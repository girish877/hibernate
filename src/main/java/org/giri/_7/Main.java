package org.giri._7;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {

	public static void main(String[] args) {

		TransportOpted opted1 = new TransportOpted();
		opted1.setModeType("Bus");
		opted1.setFare(2200);
		
		
		TransportOpted opted2 = new TransportOpted();
		opted2.setModeType("Cab");
		opted2.setFare(4000);
		
		TransportOpted opted3 = new TransportOpted();
		opted3.setModeType("Own");
		opted3.setFare(0);
		
		EmployeeOneToManyOrManyToOne employeeOneToManyOrManyToOne = new EmployeeOneToManyOrManyToOne();
		employeeOneToManyOrManyToOne.setName("Gireesh");
		employeeOneToManyOrManyToOne.getTrList().add(opted1);
		employeeOneToManyOrManyToOne.getTrList().add(opted2);
		employeeOneToManyOrManyToOne.getTrList().add(opted3);
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.save(employeeOneToManyOrManyToOne);
		
		session.getTransaction().commit();
		session.close();
		
		sessionFactory.close();
				
	}

}
