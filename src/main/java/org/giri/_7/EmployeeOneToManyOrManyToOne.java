package org.giri._7;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class EmployeeOneToManyOrManyToOne {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer eid;
	private String name;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<TransportOpted> trList = new ArrayList<>();

	public Integer getEid() {
		return eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TransportOpted> getTrList() {
		return trList;
	}

	public void setTrList(List<TransportOpted> trList) {
		this.trList = trList;
	}
	
}
