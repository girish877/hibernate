package org.giri._3;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
	
	public static void main(String[] args) {
		
		Address address = new Address();
		address.setDoorNo("C1305");
		address.setStreetName("ETA THE GARDENS");
		address.setZipcode(560023l);
		
		EmployeeEmbaded employeeEmbaded = new EmployeeEmbaded();
		employeeEmbaded.setName("Gireesh");
		
		employeeEmbaded.setAddress(address);
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		
		session.save(employeeEmbaded);
		
		session.getTransaction().commit();
		
		session.close();
		
		sessionFactory.close();
	}

}
